# view-csv

this is a very simple shell script used to browse .csv and other text-delimited datafiles from the command line.

type

% view-csv -h

to see a list of options.

it is based on a discussion i found here:

http://stackoverflow.com/questions/1875305/command-line-csv-viewer
